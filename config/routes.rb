Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  get 'news/index'
  get 'admin', to: 'admin#index'
  root 'news#index'
end
