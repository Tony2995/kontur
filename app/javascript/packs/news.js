import ActionCable from 'actioncable'
import Vue from 'vue'
document.addEventListener('DOMContentLoaded', () => {
    let app = new Vue({
        el: '#message',
        data: {
            title: "",
            description: "",
            date: ""
        }
    });



  window.$ = window.jQuery = require("jquery");


  let cable = ActionCable.createConsumer();

  cable.subscriptions.create("WebNotificationsChannel", {

      received: function(data) {

        app.title = data.message.title
        app.description = data.message.description
        app.date = data.message.date


        return $('#message').prepend(data['message']);
      }
  });

});
