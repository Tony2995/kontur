class GetNewsWorker
  include Sidekiq::Worker
  include Sidetiq::Schedulable

  # recurrence { minutely(1) }
  recurrence { secondly(20) }

  def perform
    latest_news = self.get_news.items[rand(5)]

    require 'digest/md5'
    checksum = Digest::MD5.hexdigest(latest_news.description)

    unless redis.exists(checksum)
      redis.hmset(
        'current',
        'title', latest_news.title,
        'description', latest_news.description,
        'date', latest_news.date,
        'checksum', checksum
      )
    end

    current_news = redis.hgetall('current')

    ActionCable.server.broadcast 'web_notifications_channel', message: current_news
  end

  def get_news
    require 'rss'
    RSS::Parser.parse('https://news.yandex.ru/index.rss', true)
  end

  private

  def redis
    $redis
  end
end
